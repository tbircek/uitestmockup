# UITestMockup #

Following software and hardware needed:

* [IPScom Communications Software](http://www.beckwithelectric.com/products/s-7600.html)
* [M-7679 Recloser Control](http://www.beckwithelectric.com/products/m-7679.html)

### What is this repository for? ###

* UITestMockup is a presentation to replace manual testing of [Beckwith  products](http://www.beckwithelectric.com/products/). 
* Version: 1.0.0.0

### How do I get set up? ###

* Download and install [IPScom Communications Software](http://www.beckwithelectric.com/products/s-7600.html).
* Establish TCP/IP communication to [M-7679 Recloser Control](http://www.beckwithelectric.com/products/m-7679.html).
* You will need to build this project using [Visual Studio 2017 Enterprise Edition](https://www.visualstudio.com/downloads/).
* After building the project select and run tests.

### Who do I talk to? ###

* [Contact us](http://www.beckwithelectric.com/contact/index.htm)