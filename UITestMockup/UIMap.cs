﻿namespace UITestMockup
{
    using Microsoft.VisualStudio.TestTools.UITesting;
    using Microsoft.VisualStudio.TestTools.UITesting.WinControls;
    using System.Drawing;
    using Mouse = Microsoft.VisualStudio.TestTools.UITesting.Mouse;

    public partial class UIMap
    {
        ///// <summary>
        ///// IPSlogicEditorInsertORGate
        ///// </summary>
        //public void IPSlogicEditorInsertORGate()
        //{
        //    #region Variable Declarations
        //    WpfCustom OldORGate = this.UIIPSlogicEditorModifiWindow.UISpGatesCustom.UIGatesExpander.UITbOrCustom;
        //    WpfCustom NewORGate = UIIPSlogicEditorModifiWindow.UIItemCustom1.UIGCScrollerPane.UIGateRootCustom;
        //    WpfExpander GateHolder = this.UIIPSlogicEditorModifiWindow.UISpGatesCustom.UISvGatesPane.UIGatesExpander;
        //    WpfText Profile_1_Logic_1 = this.UIIPSlogicEditorModifiWindow.UIItemCustom1.UIGateRootCustom1.UIProfile_1Logic_1Text;
        //    #endregion

        //    // Move 'ORGate' custom control to custom control
        //    GateHolder.Expanded = true;
        //    OldORGate.EnsureClickable(OldORGate.GetClickablePoint());
        //    Mouse.StartDragging(OldORGate, OldORGate.GetClickablePoint());
        //    Mouse.Move(OldORGate, new Point(0, 0));
        //    Mouse.StopDragging(OldORGate);

        //    GateHolder.Expanded = false;
        //    Mouse.StartDragging(NewORGate, NewORGate.GetClickablePoint());
        //    Mouse.StopDragging(NewORGate, 422, 80);

        //}

        /// <summary>
        /// IPSlogicClose
        /// </summary>
        public void IPSlogicClose()
        {
            #region Variable Declarations
            WinButton uIExitButton = this.UIIPSLogicWindow.UIExitWindow.UIExitButton;
            #endregion
            
            // Click 'Exit' button
            Mouse.Click(uIExitButton, ControlClickable(uIExitButton)); // new Point(72, 19));
        }

        private static Point ControlClickable(WinButton uIExitButton)
        {
            Point pt = new Point();
            for (int uiRefresh = 0; uiRefresh < 3; uiRefresh++)
            {
                do
                {
                    uIExitButton.Find();
                    uIExitButton.DrawHighlight();
                    uIExitButton.Find();
                } while (!uIExitButton.TryGetClickablePoint(out pt));
                Playback.Wait(2000);
            }
            return pt;
        }
    }
}
