﻿using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace UITestMockup
{
    /// <summary>
    /// Summary description for F50P.ABC Test.
    /// </summary>
    [CodedUITest]
    public class F50P_ABC_1_Test
    {
        IPSlogicF27xPickupTest start = new IPSlogicF27xPickupTest();

        public F50P_ABC_1_Test()
        {
        }


        // [DataSource("System.Data.Odbc", "Dsn=ExcelFiles;Driver={Microsoft Excel Driver (*.xlsx)};dbq=|DataDirectory|\\f50p.xlsx;defaultdir=.;driverid=790;maxbuffersize=2048;pagetimeout=5;readonly=true", "Sheet1$", DataAccessMethod.Sequential), DeploymentItem("Sheet1.xlsx"), TestMethod]
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", 
                    "|DataDirectory|\\test-values\\f50p.csv", "f50p#csv", 
                        DataAccessMethod.Sequential), 
                        DeploymentItem("f50p.csv"), TestMethod]
        public void F50PSetpointTest()
        {
            // F50P Pickup value.
            this.UIMap.UIItem50PPhaseInstantaWindow.UIM_EditWindow.UIM_EditEdit.Text = TestContext.DataRow["Pickup"].ToString();
            // this.UIMap.F50P_Setpoints_Pickup();

            // F50P Definite Time value.
            this.UIMap.UIItem50PPhaseInstantaWindow.UIM_EditWindow1.UIM_EditEdit.Text = TestContext.DataRow["Definite Time"].ToString();
            // this.UIMap.F50P_Setpoints_DefiniteTime();

            // F50P Outputs value.            
            this.UIMap.UIItem50PPhaseInstantaWindow.UIItem1Window.UIItem1CheckBox.Checked = Convert.ToBoolean(TestContext.DataRow["O-1"]);
            this.UIMap.UIItem50PPhaseInstantaWindow.UIItem2Window.UIItem2CheckBox.Checked = Convert.ToBoolean(TestContext.DataRow["O-2"]);
            this.UIMap.UIItem50PPhaseInstantaWindow.UIItem3Window.UIItem3CheckBox.Checked = Convert.ToBoolean(TestContext.DataRow["O-3"]);
            this.UIMap.UIItem50PPhaseInstantaWindow.UIItem4Window.UIItem4CheckBox.Checked = Convert.ToBoolean(TestContext.DataRow["O-4"]);
            this.UIMap.UIItem50PPhaseInstantaWindow.UIItem5Window.UIItem5CheckBox.Checked = Convert.ToBoolean(TestContext.DataRow["O-5"]);
            this.UIMap.UIItem50PPhaseInstantaWindow.UIItem6Window.UIItem6CheckBox.Checked = Convert.ToBoolean(TestContext.DataRow["O-6"]);
            this.UIMap.UIItem50PPhaseInstantaWindow.UIItem7Window.UIItem7CheckBox.Checked = Convert.ToBoolean(TestContext.DataRow["O-7"]);
            this.UIMap.UIItem50PPhaseInstantaWindow.UIItem8Window.UIItem8CheckBox.Checked = Convert.ToBoolean(TestContext.DataRow["O-8"]);
            this.UIMap.UIItem50PPhaseInstantaWindow.UIItem9Window.UIItem9CheckBox.Checked = Convert.ToBoolean(TestContext.DataRow["O-9"]);
            this.UIMap.UIItem50PPhaseInstantaWindow.UIItem10Window.UIItem10CheckBox.Checked = Convert.ToBoolean(TestContext.DataRow["O-10"]);
            this.UIMap.UIItem50PPhaseInstantaWindow.UIItem11Window.UIItem11CheckBox.Checked = Convert.ToBoolean(TestContext.DataRow["O-11"]);
            this.UIMap.UIItem50PPhaseInstantaWindow.UIItem12Window.UIItem12CheckBox.Checked = Convert.ToBoolean(TestContext.DataRow["O-12"]);
            // this.UIMap.F50P_Setpoints_Outputs();
        }

        #region Additional test attributes

        // You can use the following additional attributes as you write your tests:

        //Use TestInitialize to run code before running each test 
        [TestInitialize()]
        public void MyTestInitialize()
        {
            // To generate code for this test, select "Generate Code for Coded UI Test" from the shortcut menu and select one of the menu items.
            start.MyTestInitialize();
            this.UIMap.F50P_Setpoints_Open();
            this.UIMap.F50P_Setpoints_Enable();
        }

        //Use TestCleanup to run code after each test has run
        [TestCleanup()]
        public void MyTestCleanup()
        {
            // To generate code for this test, select "Generate Code for Coded UI Test" from the shortcut menu and select one of the menu items.
            this.UIMap.F50P_Setpoints_Save();
            this.UIMap.F50P_Setpoints_Close();
            start.MyTestCleanup();
        }

        #endregion

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }
        private TestContext testContextInstance;

        public UIMap UIMap
        {
            get
            {
                if (this.map == null)
                {
                    this.map = new UIMap();
                }

                return this.map;
            }
        }

        private UIMap map;
    }
}
