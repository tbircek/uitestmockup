﻿using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UnitTesting;


namespace UITestMockup
{
    /// <summary>
    /// Summary description for CodedUITest1
    /// </summary>
    [CodedUITest]
    public class IPSlogicF27xPickupTest
    {
        public IPSlogicF27xPickupTest()
        {
        }

        [TestMethod]
        public void F27xPickupCodedUITest()
        {
            // To generate code for this test, select "Generate Code for Coded UI Test" from the shortcut menu and select one of the menu items.

            IPSLogicPreSet();
            UIMap.IPSlogicDisplayInputOpen();
            UIMap.IPSlogicFilterF27();
            UIMap.IPSlogicInsertF271APickup();
            UIMap.IPSlogicDisplayInputClickInsert();
            UIMap.IPSlogicConnectF271APickup();
            IPSLogicSave();
            IPSLogicPreSet();
            UIMap.IPSlogicDisplayInputOpen();
            UIMap.IPSlogicFilterF27();
            UIMap.IPSlogicInsertF271BPickup();
            UIMap.IPSlogicDisplayInputClickInsert();
            UIMap.IPSlogicConnectF271BPickup();
            IPSLogicSave();
        }

        // [TestMethod]
        private void IPSLogicPreSet()
        {
            UIMap.IPSlogicOpen();
            UIMap.IPSlogicEnable1();
            UIMap.IPSlogicResetOutputs();
            UIMap.IPSlogicOpenEditor();
            UIMap.IPSlogicResetEditor();
            UIMap.IPSlogicOpenEditor();
        }

        // [TestMethod]
        private void IPSLogicSave()
        {
            UIMap.IPSlogicEditorSaveFile();
            UIMap.IPSlogicSetOutputs();
            UIMap.IPSlogicSave1();
            UIMap.IPSlogicClose();
        }

        #region Additional test attributes

        // You can use the following additional attributes as you write your tests:

        //Use TestInitialize to run code before running each test 
        [TestInitialize()]
        public void MyTestInitialize()
        {
            // To generate code for this test, select "Generate Code for Coded UI Test" from the shortcut menu and select one of the menu items.
            UIMap.StartIPScom();
            UIMap.TcpIpConnect();
            UIMap.SetpointOpen();
            UIMap.SelectProfile1();
        }

        //Use TestCleanup to run code after each test has run
        [TestCleanup()]
        public void MyTestCleanup()
        {
            // To generate code for this test, select "Generate Code for Coded UI Test" from the shortcut menu and select one of the menu items.
            UIMap.SetpointClose();
            UIMap.CloseIPScom();
        }

        #endregion

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }
        private TestContext testContextInstance;

        public UIMap UIMap
        {
            get
            {
                if (map == null)
                {
                    map = new UIMap();
                }

                return map;
            }
        }

        private UIMap map;
    }
}
